package com.example.kotlinmovie.vo

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}