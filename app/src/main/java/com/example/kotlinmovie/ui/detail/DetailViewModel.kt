package com.example.kotlinmovie.ui.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.kotlinmovie.data.MainRepository
import com.example.kotlinmovie.data.local.model.EntityMovie
import com.example.kotlinmovie.vo.Resource
import javax.inject.Inject


class DetailViewModel @Inject constructor(private val mainRepository: MainRepository) :
    ViewModel() {

    private var _movieId: Int = 0

    private val mainViewModel by lazy {
        val movieDetail = MutableLiveData<Resource<EntityMovie>>()
        mainRepository.getDetailMovie(_movieId).observeForever { movieDetail.value = it }
        return@lazy movieDetail
    }

    fun getDetailMovie(): LiveData<Resource<EntityMovie>> = mainViewModel

    fun start(idMovie: Int) {
        _movieId = idMovie
    }
}
