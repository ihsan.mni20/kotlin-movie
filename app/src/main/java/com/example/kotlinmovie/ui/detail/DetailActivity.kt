package com.example.kotlinmovie.ui.detail

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.kotlinmovie.BuildConfig
import com.example.kotlinmovie.R
import com.example.kotlinmovie.data.local.model.EntityMovie
import com.example.kotlinmovie.viewmodel.ViewModelFactory
import com.example.kotlinmovie.vo.Status
import com.squareup.picasso.Picasso
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_detail.*
import kotlinx.android.synthetic.main.content_view.*
import kotlinx.android.synthetic.main.movie_wrapper.*
import javax.inject.Inject


class DetailActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    @Inject
    lateinit var detailViewModel: DetailViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        val idMovie = intent.getIntExtra("ExtrasMovie", 0)

        setupViewModel()
        setupObserver(idMovie)
    }

    private fun setupViewModel() {

        detailViewModel = ViewModelProvider(this, viewModelFactory)[DetailViewModel::class.java]
    }

    private fun setupObserver(idMovie: Int) {

        detailViewModel.start(idMovie)

        detailViewModel.getDetailMovie().observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    loading.visibility = View.GONE
                    it.data?.let { movie -> renderData(movie) }
                    content_detail.visibility = View.VISIBLE
                }
                Status.LOADING -> {
                    content_detail.visibility = View.GONE
                    loading.visibility = View.VISIBLE
                }
                Status.ERROR -> {
                    loading.visibility = View.GONE
                    Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                }
            }
        })
    }

    private fun renderData(movie: EntityMovie) {

        detail_title.text = movie.title

        movie.posterPath?.let {
            Picasso.get().load(BuildConfig.IMAGE_URL + movie.posterPath).into(detail_img_poster)
        }

        movie.backdropPath?.let {
            Picasso.get().load(BuildConfig.IMAGE_URL + movie.backdropPath).into(detail_img)
        }

        rating_bar.rating = movie.voteAverage?.div(2)?.toFloat()!!
        detail_overview.text = movie.overview
        detail_release_date.text = movie.releaseDate
        detail_vote_average.text = movie.voteAverage!!.div(2).toFloat().toString()
        vote_count.text = movie.voteCount.toString()

    }
}