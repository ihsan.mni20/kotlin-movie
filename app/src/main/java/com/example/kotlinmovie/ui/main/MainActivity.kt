package com.example.kotlinmovie.ui.main

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.GridLayoutManager
import com.example.kotlinmovie.R
import com.example.kotlinmovie.data.local.model.EntityMovie
import com.example.kotlinmovie.viewmodel.ViewModelFactory
import com.example.kotlinmovie.vo.Status
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : DaggerAppCompatActivity() {

    private lateinit var adapter: MainAdapter

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    @Inject
    lateinit var mainViewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        setupUI()
        setupViewModel()
        setupObserver()
    }

    private fun renderList(entityMovies: List<EntityMovie>) {
        adapter.addData(entityMovies)
        adapter.notifyDataSetChanged()
    }

    private fun setupUI() {
        listMovie.layoutManager = GridLayoutManager(this, 2)
        adapter = MainAdapter(arrayListOf())
        listMovie.addItemDecoration(
            DividerItemDecoration(
                listMovie.context,
                (listMovie.layoutManager as GridLayoutManager).orientation
            )
        )

        listMovie.adapter = adapter
    }

    private fun setupViewModel() {
        mainViewModel = ViewModelProvider(this, viewModelFactory)[MainViewModel::class.java]
    }

    private fun setupObserver() {
        mainViewModel.getMovie().observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    progressBar.visibility = View.GONE
                    it.data?.let { movies -> renderList(movies) }
                    listMovie.visibility = View.VISIBLE
                }
                Status.LOADING -> {
                    progressBar.visibility = View.VISIBLE
                    listMovie.visibility = View.GONE
                }
                Status.ERROR -> {
                    progressBar.visibility = View.GONE
                    Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                }
            }
        })
    }
}