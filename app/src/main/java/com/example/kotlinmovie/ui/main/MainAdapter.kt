package com.example.kotlinmovie.ui.main

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.kotlinmovie.BuildConfig
import com.example.kotlinmovie.R
import com.example.kotlinmovie.data.local.model.EntityMovie
import com.example.kotlinmovie.ui.detail.DetailActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.list_item.view.*

class MainAdapter(private val entityMovie: ArrayList<EntityMovie>) :
    RecyclerView.Adapter<MainAdapter.DataViewHolder>() {

    class DataViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(entityMovie: EntityMovie) {
            with(itemView) {
                textTitle.text = entityMovie.title
                textRelease.text = entityMovie.releaseDate
                entityMovie.posterPath?.let {
                    Picasso.get().load(BuildConfig.IMAGE_URL + it).into(itemView.imagePoster)
                    //Log.d("poster ", BuildConfig.IMAGE_URL + it)
                }

                setOnClickListener {
                    val intent = Intent(context, DetailActivity::class.java).apply {
                        putExtra("ExtrasMovie", entityMovie.movieId)
                    }
                    context.startActivity(intent)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataViewHolder =
        DataViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.list_item, parent, false)
        )

    override fun getItemCount(): Int = entityMovie.size

    override fun onBindViewHolder(holder: DataViewHolder, position: Int) {
        holder.bind(entityMovie[position])
    }

    fun addData(list: List<EntityMovie>) {
        entityMovie.addAll(list)
    }
}