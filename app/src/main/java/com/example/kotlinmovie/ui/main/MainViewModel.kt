package com.example.kotlinmovie.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.kotlinmovie.data.MainRepository
import com.example.kotlinmovie.data.local.model.EntityMovie
import com.example.kotlinmovie.vo.Resource
import javax.inject.Inject

class MainViewModel @Inject constructor(private val mainRepository: MainRepository) : ViewModel() {

    private val mainViewModel by lazy {
        val movieList = MutableLiveData<Resource<List<EntityMovie>>>()
        mainRepository.getMovie().observeForever { movieList.value = it }
        return@lazy movieList
    }

    fun getMovie(): LiveData<Resource<List<EntityMovie>>> = mainViewModel

}





