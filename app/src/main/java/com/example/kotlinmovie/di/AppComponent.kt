package com.example.kotlinmovie.di

import android.app.Application
import com.example.kotlinmovie.MovieApp
import com.example.kotlinmovie.di.module.ActivityModule
import com.example.kotlinmovie.di.module.AppModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [AndroidSupportInjectionModule::class, AppModule::class, ActivityModule::class]
)
interface AppComponent : AndroidInjector<MovieApp> {

    override fun inject(application: MovieApp)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder
        fun build(): AppComponent
    }
}