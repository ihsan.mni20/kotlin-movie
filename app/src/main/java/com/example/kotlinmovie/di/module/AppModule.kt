package com.example.kotlinmovie.di.module

import android.app.Application
import androidx.room.Room
import com.example.kotlinmovie.BuildConfig
import com.example.kotlinmovie.data.local.dao.MovieDao
import com.example.kotlinmovie.data.local.db.AppDatabase
import com.example.kotlinmovie.data.remote.services.MovieService
import com.example.kotlinmovie.data.remote.services.MovieServiceFactory
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module(includes = [ViewModelModule::class])
class AppModule {

    @Singleton
    @Provides
    fun provideDb(app: Application): AppDatabase {
        return Room
            .databaseBuilder(app, AppDatabase::class.java, "movies.db")
            .fallbackToDestructiveMigration()
            .build()
    }

    @Singleton
    @Provides
    fun provideMovieDao(db: AppDatabase): MovieDao {
        return db.movieDao()
    }

    @Singleton
    @Provides
    fun provideMovieService(): MovieService {
        return MovieServiceFactory.create(
            BuildConfig.BASE_URL,
            BuildConfig.API_MOVIEDB
        )
    }


}