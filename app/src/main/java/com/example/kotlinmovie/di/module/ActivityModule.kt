package com.example.kotlinmovie.di.module

import com.example.kotlinmovie.ui.detail.DetailActivity
import com.example.kotlinmovie.ui.main.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {

    @ContributesAndroidInjector
    internal abstract fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector
    internal abstract fun bindDetailActivity(): DetailActivity

}