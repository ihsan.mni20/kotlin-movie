package com.example.kotlinmovie.data.remote.model

data class MovieListModel(
    val results: List<MovieModel>
)
