package com.example.kotlinmovie.data.local.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.kotlinmovie.data.local.dao.MovieDao
import com.example.kotlinmovie.data.local.model.EntityMovie

@Database(
    entities = [EntityMovie::class], version = 1,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun movieDao(): MovieDao
}