package com.example.kotlinmovie.data.remote


import com.example.kotlinmovie.data.remote.model.MovieListModel
import com.example.kotlinmovie.data.remote.model.MovieModel
import com.example.kotlinmovie.data.remote.services.MovieService
import io.reactivex.Single
import javax.inject.Inject

open class RemoteDataSource @Inject constructor(private val movieService: MovieService) {
    fun getMovie(): Single<MovieListModel> = movieService.getMovie()
    fun getDetailMovie(movieId: Int): Single<MovieModel> = movieService.getDetailMovie(movieId)
}