package com.example.kotlinmovie.data.remote.services

enum class StatusResponse {
    SUCCESS,
    EMPTY,
    ERROR
}
