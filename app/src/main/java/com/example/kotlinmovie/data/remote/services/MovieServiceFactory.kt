package com.example.kotlinmovie.data.remote.services

import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object MovieServiceFactory {
    private const val OK_HTTP_TIMEOUT = 60L

    fun create(baseUrl: String, apiKey: String): MovieService {

        val headerInterceptor = Interceptor { chain ->
            val original = chain.request()

            val url = original.url()
                .newBuilder()
                .addQueryParameter("api_key", apiKey)
                .build()
            val request: Request = chain.request().newBuilder().url(url).build()
            chain.proceed(request)
        }

        val okHttpClient: OkHttpClient = OkHttpClient().newBuilder()
            .addInterceptor(headerInterceptor)
            .connectTimeout(OK_HTTP_TIMEOUT, TimeUnit.SECONDS)
            .readTimeout(OK_HTTP_TIMEOUT, TimeUnit.SECONDS)
            .build()

        val retrofit = Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .baseUrl(baseUrl)
            .build()

        return retrofit.create(MovieService::class.java)
    }
}