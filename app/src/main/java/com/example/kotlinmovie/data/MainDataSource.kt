package com.example.kotlinmovie.data

import androidx.lifecycle.LiveData
import com.example.kotlinmovie.data.local.model.EntityMovie
import com.example.kotlinmovie.vo.Resource

interface MainDataSource {
    fun getMovie(): LiveData<Resource<List<EntityMovie>>>
    fun getDetailMovie(movieId: Int): LiveData<Resource<EntityMovie>>
}