package com.example.kotlinmovie.data.local.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.example.kotlinmovie.data.local.model.EntityMovie


@Dao
interface MovieDao {

    @Query("SELECT * FROM movie_table")
    fun getAll(): LiveData<List<EntityMovie>>

    @Query("SELECT * FROM movie_table WHERE movieId = :movieIds")
    fun loadByIds(movieIds: Int): LiveData<EntityMovie>

    @Insert(onConflict = REPLACE)
    fun save(entityMovies: List<EntityMovie>)

    @Delete
    fun delete(entityMovie: EntityMovie)
}