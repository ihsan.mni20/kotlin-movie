package com.example.kotlinmovie.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.kotlinmovie.AppExecutors
import com.example.kotlinmovie.data.local.LocalDataSource
import com.example.kotlinmovie.data.local.model.EntityMovie
import com.example.kotlinmovie.data.remote.RemoteDataSource
import com.example.kotlinmovie.data.remote.model.MovieModel
import com.example.kotlinmovie.data.remote.services.ApiResponse
import com.example.kotlinmovie.vo.Resource
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MainRepository @Inject constructor(
    private val remoteDataSource: RemoteDataSource,
    private val executor: AppExecutors,
    private val localDataSource: LocalDataSource
) : MainDataSource {

    private val compositeDisposable = CompositeDisposable()

    override fun getMovie(): LiveData<Resource<List<EntityMovie>>> {

        return object : NetworkBoundResource<List<EntityMovie>, List<MovieModel>>(executor) {

            override fun loadFromDb(): LiveData<List<EntityMovie>> = localDataSource.getAllMovie()

            override fun shouldFetch(data: List<EntityMovie>?): Boolean =
                data == null || data.isEmpty()

            override fun createCall(): LiveData<ApiResponse<List<MovieModel>>> {
                val movie = MutableLiveData<ApiResponse<List<MovieModel>>>()
                compositeDisposable.add(
                    remoteDataSource.getMovie()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({ movieList ->
                            movie.value = ApiResponse.success(movieList.results)
                        }, {
                            ApiResponse.error("movieList.results", null)
                        })
                )
                return movie
            }

            override fun saveCallResult(item: List<MovieModel>) {
                val movieList = ArrayList<EntityMovie>()
                for (response in item) {
                    val movie =
                        EntityMovie(
                            response.id,
                            response.posterPath,
                            response.releaseDate,
                            response.overview,
                            response.title,
                            response.voteAverage,
                            response.voteCount,
                            response.backdropPath
                        )
                    movieList.add(movie)
                }

                localDataSource.insertMovie(movieList)
            }

        }.asLiveData()
    }

    override fun getDetailMovie(movieId: Int): LiveData<Resource<EntityMovie>> {

        return object : NetworkBoundResource<EntityMovie, MovieModel>(executor) {

            override fun loadFromDb(): LiveData<EntityMovie> =
                localDataSource.getDetailMovie(movieId)

            override fun shouldFetch(data: EntityMovie?): Boolean =
                data == null

            override fun createCall(): LiveData<ApiResponse<MovieModel>> {
                val movie = MutableLiveData<ApiResponse<MovieModel>>()
                compositeDisposable.add(
                    remoteDataSource.getDetailMovie(movieId)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({ movieList ->
                            movie.value = ApiResponse.success(movieList)
                        }, {

                        })
                )
                return movie
            }

            override fun saveCallResult(item: MovieModel) {
                val movieList = ArrayList<EntityMovie>()
                val movie = EntityMovie(
                    item.id,
                    item.posterPath,
                    item.releaseDate,
                    item.overview,
                    item.title,
                    item.voteAverage,
                    item.voteCount,
                    item.backdropPath
                )
                movieList.add(movie)
                localDataSource.insertMovie(movieList)
            }

        }.asLiveData()
    }


}


