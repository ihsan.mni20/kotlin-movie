package com.example.kotlinmovie.data.local.model

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "movie_table")
data class EntityMovie(
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "movieId") var movieId: Int?,
    @ColumnInfo(name = "poster_path") var posterPath: String?,
    @ColumnInfo(name = "release_date") var releaseDate: String?,
    @ColumnInfo(name = "overview") var overview: String?,
    @ColumnInfo(name = "title") var title: String?,
    @ColumnInfo(name = "vote_average") var voteAverage: Double?,
    @ColumnInfo(name = "vote_count") var voteCount: Int?,
    @ColumnInfo(name = "backdrop_path") var backdropPath: String?

)



