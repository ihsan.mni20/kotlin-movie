package com.example.kotlinmovie.data.local

import androidx.lifecycle.LiveData
import com.example.kotlinmovie.data.local.dao.MovieDao
import com.example.kotlinmovie.data.local.model.EntityMovie
import javax.inject.Inject

class LocalDataSource @Inject constructor(private val movieDao: MovieDao) {
    fun getAllMovie(): LiveData<List<EntityMovie>> = movieDao.getAll()
    fun getDetailMovie(movieIds: Int): LiveData<EntityMovie> = movieDao.loadByIds(movieIds)
    fun insertMovie(entityMovieList: ArrayList<EntityMovie>) = movieDao.save(entityMovieList)
}