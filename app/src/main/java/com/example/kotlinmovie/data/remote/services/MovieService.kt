package com.example.kotlinmovie.data.remote.services

import com.example.kotlinmovie.data.remote.model.MovieListModel
import com.example.kotlinmovie.data.remote.model.MovieModel
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path


interface MovieService {
    @GET("discover/movie")
    fun getMovie(): Single<MovieListModel>

    @GET("movie/{movie_id}")
    fun getDetailMovie(@Path("movie_id") id: Int): Single<MovieModel>
}