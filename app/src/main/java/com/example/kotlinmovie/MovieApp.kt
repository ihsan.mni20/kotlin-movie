package com.example.kotlinmovie

import com.example.kotlinmovie.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import dagger.android.DispatchingAndroidInjector
import javax.inject.Inject

class MovieApp : DaggerApplication() {

    private lateinit var instance: MovieApp
    private val createComponent = DaggerAppComponent.builder().application(this).build()

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    override fun onCreate() {
        super.onCreate()
        instance = this

    }


    override fun applicationInjector(): AndroidInjector<out DaggerApplication> = createComponent


    override fun androidInjector(): AndroidInjector<Any>? = dispatchingAndroidInjector


}


